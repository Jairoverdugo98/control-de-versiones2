##  Contenidos
Un sistema de control de versiones debe proporcionar las siguientes caracteristicas:

* Mecanismo de almacenamiento de los elementos que deba gestionar (ej. archivos de texto, imágenes, documentación...)

* Posibilidad de realizar cambios sobre los elementos almacenados (ej. modificaciones parciales, añadir, borrar, renombrar o mover elementos)

* Registro histórico de las acciones realizadas con cada elemento o conjunto de elementos (normalmente pudiendo volver o extraer un estado anterior del producto).



## Softwares
- **Git**: Es un modelo de repositorio distribuido compatible con sistemas y protocolos existentes como HTTP, FTP, SSH y es capaz de manejar eficientemente proyectos pequeños a grandes.

- **CVS**: Es un modelo de repositorio cliente-servidor donde varios desarrolladores pueden trabajar en el mismo proyecto en paralelo.

- **Apache Subversion (SVN)**: Es un modelo de repositorio cliente-servidor donde los directorios están versionados junto con las operaciones de copia, eliminación, movimiento y cambio de nombre.

- **Mercurial**: Es una herramienta distribuida de control de versiones que está escrita en Python y destinada a desarrolladores de software.

- **Monotone**: Está escrito en C ++ y es una herramienta para el control de versiones distribuido. El sistema operativo que admite incluye Unix, Linux, BSD, Mac OS X y Windows. Utiliza un protocolo personalizado muy eficiente y robusto llamado Netsync.



![image](/img/control1.png)


